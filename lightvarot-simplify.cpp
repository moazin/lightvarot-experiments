#include <Path.h>
#include <PathElem.h>
#include <Polygon.h>
#include <PointSequence.h>
#include <PathElemMove.h>
#include <PathElemLine.h>
#include <PathElemCurve.h>
#include <PathElemClose.h>
#include <2geom/point.h>
#include <2geom/path-sink.h>
#include <2geom/pathvector.h>
#include <2geom/svg-path-parser.h>
#include <2geom/svg-path-writer.h>

Geom::Point point(MPoint<float> pt) {
  return Geom::Point(pt._x, pt._y);
}
MPoint<float> point(Geom::Point pt) {
  return MPoint<float>(pt.x(), pt.y());
}

class LightvarotPath: public Geom::PathSink {
  private:
    Path *path;
    bool last_point_set;
    Geom::Point last_pt;
  public:
    LightvarotPath() {
      path = new Path;
      last_point_set = false;
    }
    ~LightvarotPath() {
      delete path;
    }
    void moveTo(Geom::Point const &p) {
      path->moveto(point(p));
      last_point_set = true;
      last_pt = p;
    }
    void lineTo(Geom::Point const &p) {
      if(last_point_set)
      {
        path->lineto(point(p));
        last_pt = p;
      }
    }
    void curveTo(Geom::Point const &c0, Geom::Point const &c1, Geom::Point const &p) {
      if(last_point_set)
      {
        path->curveto(point(c0), point(c1), point(p));
        last_pt = p;
      }
    }
    std::vector<Geom::Point> quadToCubic(Geom::Point p0, Geom::Point c, Geom::Point p1) {
      std::vector<Geom::Point> points;
      points.push_back(p0);
      points.push_back(p0 + (2.0/3.0)*(c - p0));
      points.push_back(p1 + (2.0/3.0)*(c - p1));
      points.push_back(p1);
      return points;
    }
    void quadTo(Geom::Point const &c, Geom::Point const &p1) {
      if(last_point_set)
      {
        Geom::Point p0 = last_pt;
        path->curveto(point(p0 + (2.0/3.0)*(c - p0)), point(p1 + (2.0/3.0)*(c - p1)), point(p1));
      }
    }
    void arcTo(Geom::Coord rx, Geom::Coord ry, Geom::Coord angle,
               bool large_arc, bool sweep, Geom::Point const &p) {
      // Lightvarot doesn't take elliptical arcs :-P
    }
    void closePath() {
      path->close();
    }

    void flush() {
      last_point_set = false;
    }

    Path getPath() {
      return *path;
    }

};

std::string lightvarot_path_to_svg(Path *path) {
  Geom::SVGPathWriter pathWriter;
  int no_descrs = path->_elems.nb();
  for(int i = 0; i < no_descrs; i++)
  {
    PathElem* elem = path->_elems[i];
    PathElemMove* move_elem = dynamic_cast<PathElemMove*>(elem);
    PathElemLine* line_elem = dynamic_cast<PathElemLine*>(elem);
    PathElemCurve* curve_elem = dynamic_cast<PathElemCurve*>(elem);
    PathElemClose* close_elem = dynamic_cast<PathElemClose*>(elem);
    if(move_elem)
      pathWriter.moveTo(point(move_elem->_pt));
    else if(line_elem)
      pathWriter.lineTo(point(line_elem->_en));
    else if(curve_elem)
      pathWriter.curveTo(point(curve_elem->_p1), point(curve_elem->_p2), point(curve_elem->_en));
    else if(close_elem)
      pathWriter.closePath();
  }
  return pathWriter.str();
}

int main(void) {
  std::string d = "m 34.242025,148.09759 c 0.691324,-0.14334 1.424073,-0.0454 2.093571,-0.29323 0.277489,-0.1027 0.555693,-0.26615 0.693845,-0.54055 0.245178,-0.48698 0.162469,-1.0883 0.462391,-1.54798 0.384218,-0.58888 0.884781,-1.08879 1.377159,-1.58626 0.58397,-0.59001 1.164145,-1.20073 1.567403,-1.93454 0.683218,-1.24326 0.994656,-2.64122 1.532708,-3.94543 0.263125,-0.6378 0.568327,-1.25731 0.874448,-1.87524 0.922222,-1.86159 1.907633,-3.69117 2.804145,-5.56586 1.277094,-2.67052 2.524996,-5.36346 3.481941,-8.16852 0.450638,-1.32094 0.86643,-2.65601 1.184829,-4.01498 0.448668,-1.91498 0.485279,-3.90859 1.028223,-5.80232 0.362093,-1.26294 1.147493,-2.36964 1.429431,-3.65724 0.246952,-1.12782 0.510887,-2.25635 0.63778,-3.40501 0.05785,-0.52368 0.09954,-1.07064 -0.07175,-1.57859 -0.11121,-0.32978 -0.194878,-0.66971 -0.221254,-1.01784 -0.06593,-0.87024 0.0207,-1.74318 0.106287,-2.60893 0.04567,-0.46196 0.10469,-0.924183 0.229147,-1.372969 0.226178,-0.815586 0.555945,-1.599327 0.7379,-2.428686 0.220732,-1.006107 0.300374,-2.037072 0.317889,-3.065413 0.0048,-0.283467 0.0066,-1.133987 0.0059,-0.850485 -0.0018,0.720516 0.0096,1.441172 -0.0046,2.161555 -0.03113,1.5845 -0.225323,3.177603 -0.01718,4.756584 0.191773,1.454834 0.571607,2.877424 0.993556,4.280064 0.969448,3.22264 2.192459,6.35982 3.333812,9.5234 0.61524,1.70531 1.077128,3.45961 1.599729,5.19416 0.340586,1.13043 0.690617,2.26154 1.164532,3.34478 0.263973,0.60337 0.512726,1.21778 0.85012,1.7834 0.103809,0.17403 0.210054,0.37924 0.407847,0.45672 0.153989,0.0603 0.305461,-0.0663 0.36402,-0.20643 0.08785,-0.21017 0.109467,-0.4496 0.269787,-0.62884 0.115211,-0.12881 0.258027,-0.22686 0.356742,-0.37377 0.55457,-0.82534 1.143236,-1.62942 1.626323,-2.50116 0.47701,-0.86077 1.007942,-1.69799 1.386737,-2.60712 0.09728,-0.23349 0.22942,-0.47248 0.218174,-0.73065 -0.0086,-0.19755 -0.145813,-0.35499 -0.188885,-0.54488 -0.09298,-0.40993 -0.03898,-0.83695 0.032,-1.24497 0.208549,-1.19878 0.565877,-2.366 0.945248,-3.51974 0.612023,-1.86128 1.275135,-3.70726 2.046556,-5.50922 1.635412,-3.82016 3.612933,-7.482459 5.243016,-11.305356 0.975646,-2.288101 1.953753,-4.576431 2.836287,-6.902581 0.37445,-0.986962 0.687972,-1.996677 1.102393,-2.968567 0.720958,-1.690771 1.523982,-3.344926 2.301454,-5.010053 0.892416,-1.911304 2.051841,-3.697592 2.820577,-5.662736 0.235422,-0.601817 0.420281,-1.221816 0.630995,-1.832424 0.188004,-0.544798 0.323045,-1.117609 0.610152,-1.6184 0.05982,-0.104347 0.113518,-0.36758 0.217478,-0.295259 0.08471,0.05893 -0.01873,0.194513 0.02678,0.319888 0.210847,0.580901 0.513572,1.127202 0.697108,1.718495 0.03639,0.117253 -0.0057,0.241677 -0.01087,0.362445 -0.03219,0.744272 0.02033,1.489019 0.06718,2.231486 0.163119,2.585029 0.464238,5.163955 0.47384,7.756651 0.01,2.701004 -0.236323,5.406057 0.0017,8.104016 0.416668,4.723762 1.442367,9.364505 1.9992,14.070645 0.29133,2.46221 0.473031,4.94326 0.956335,7.37711 0.320855,1.61578 0.699699,3.21947 1.087282,4.82029 0.579252,2.39246 1.177488,4.78073 1.831082,7.15418 0.217669,0.79044 0.471713,1.57062 0.76607,2.33603 0.589577,1.53306 1.288816,3.03204 1.686751,4.63137 0.134175,0.53926 0.324065,1.07894 0.325136,1.6366 2.14e-4,0.11148 0.05129,0.28151 -0.05509,0.33454 -0.107984,0.0538 -0.204441,-0.0713 -0.261507,-0.1476 -0.03935,-0.0526 -0.141853,-0.23923 -0.11882,-0.20997 0.222541,0.28271 0.400735,0.59741 0.593188,0.90071 0.237433,0.37419 0.412459,0.7963 0.714597,1.12219 0.0695,0.075 0.158439,0.22176 0.263494,0.17182 0.100625,-0.0478 0.11686,-0.17676 0.131291,-0.27477 0.02178,-0.14794 0.02553,-0.29786 0.02598,-0.44718";

  Geom::PathVector pathv = Geom::parse_svg_path(d.c_str());
  LightvarotPath lp;
  lp.feed(pathv);
  Path path = lp.getPath();
  Polygon polygon;
  polygon.setSource(&path, true);
  path.fill(&polygon, 0.5);
  PointSequence sequence;
  for(int i = 0; i < polygon._pts.nb(); i++)
  {
    sequence.addPoint(polygon._pts[i]);
  }
  Path *simplified = sequence.simplify(0.01, false);
  std::string str = lightvarot_path_to_svg(simplified);
  std::cout << str << std::endl;
}



